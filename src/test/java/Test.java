import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Test {

    public static void main(String[] args) {
        String location = System.setProperty("webdriver.chrome.driver", "C:\\Users\\zhake\\IdeaProjects\\Arbetsformedlingen\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        driver.get("https://arbetsformedlingen.se/");

        WebElement loginButton = driver.findElement(By.xpath("//*[@class='functions-nav__link js-start-login']"));
        loginButton.click();

        WebElement arbetsgivare = driver.findElement(By.xpath("//a[@href='/loggain/arbetsgivare']"));
        arbetsgivare.click();

        WebElement username = driver.findElement(By.id("username"));
        username.sendKeys("Utv_TestBengt");
        WebElement password = driver.findElement(By.id("txtLosen"));
        password.sendKeys("Abcd1234");
        WebElement loginsButton = driver.findElement(By.xpath("//*[@type='submit']"));
        loginsButton.click();
        WebElement menu = driver.findElement(By.className("i-bars"));
        menu.click();
        wait.until(ExpectedConditions.elementToBeClickable(menu));
        WebElement arbetsgivare2 = driver.findElement(By.xpath("//li[@class='main-menu__item']/a[@href='/For-arbetsgivare.html']"));
        arbetsgivare2.click();
        WebElement annonsera = driver.findElement(By.xpath("//*[@href='https://www.arbetsformedlingen.se/For-arbetsgivare/Annonsera ']"));
        annonsera.click();
        WebElement annonsera2 = driver.findElement(By.xpath("//*[@class='btn-app-link']"));
        annonsera2.click();
        driver.close();

    }
}
