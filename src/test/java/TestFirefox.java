import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestFirefox {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\zhake\\IdeaProjects\\Arbetsformedlingen\\src\\main\\resources\\geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Test
    public void testArbetsformedlingenHomepage() {
        driver.get("https://arbetsformedlingen.se/");
        WebElement headerText = driver.findElement(By.xpath("//h1[@class='heading']"));
        System.out.println(headerText);
        driver.close();
    }
}
