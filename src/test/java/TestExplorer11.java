import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class TestExplorer11 {
    public WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");
        driver = new InternetExplorerDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void  testArbetsformedlingenHomePage() {
        driver.get("https://arbetsformedlingen.se/");
        WebElement headerText = driver.findElement(By.xpath("//*[@class='heading']"));
        System.out.println(headerText);
        driver.close();

    }

}
