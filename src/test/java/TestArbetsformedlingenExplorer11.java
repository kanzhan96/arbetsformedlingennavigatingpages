import arbetsformedlingenPagesExplorer11.ArbetsformedlingenHomePageExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenLoginExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenLoginArbetsgivareExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenMinaSidorExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenForArbetsgivareExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenAnnonseraExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenHanteraAnnonserExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenSkapaAnnonsExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenAnnonseringsvillkorExplorer11;
import arbetsformedlingenPagesExplorer11.ArbetsformedlingenDirektoverfordaAnnonserExplorer11;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestArbetsformedlingenExplorer11 {
    WebDriver driver;
    ArbetsformedlingenHomePageExplorer11 objHomePage;
    ArbetsformedlingenLoginExplorer11 objLogin;
    ArbetsformedlingenLoginArbetsgivareExplorer11 objLoginArbetsgivare;
    ArbetsformedlingenMinaSidorExplorer11 objMinaSidor;
    ArbetsformedlingenForArbetsgivareExplorer11 objForArbetsgivare;
    ArbetsformedlingenAnnonseraExplorer11 objAnnonsera;
    ArbetsformedlingenHanteraAnnonserExplorer11 objHanteraAnnonser;
    ArbetsformedlingenSkapaAnnonsExplorer11 objSkapaAnnons;
    ArbetsformedlingenAnnonseringsvillkorExplorer11 objAnnonseringsvillkor;
    ArbetsformedlingenDirektoverfordaAnnonserExplorer11 objDirektöverfördaAnnonser;

    @BeforeEach
    public void setup() {
        //System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");
        WebDriverManager.iedriver().setup();
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
        driver = new InternetExplorerDriver(capabilities);
        driver.manage().window().maximize();
        driver.get("https://arbetsformedlingen.se/");
    }
    @AfterEach
    public void closeDriver() {driver.close();}
    @Test
    public void test_Arbetsformedlingen_Home_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
    }
    @Test
    public void test_Arbetsformedlingen_Login_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLoginTab();

        objLogin = new ArbetsformedlingenLoginExplorer11(driver);
        objLogin.waitForLoginPageToLoad();
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
    }
    @Test
    public void test_Arbetsformedlingen_Login_Arbetsgivare_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLoginTab();
        objLogin = new ArbetsformedlingenLoginExplorer11(driver);
        objLogin.waitForLoginPageToLoad();
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();

        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareExplorer11(driver);
        objLoginArbetsgivare.waitForLoginArbetsgivareTabToLoad();
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLoginButton();
    }
    @Test
    public void test_Arbetsformedlingen_Mina_Sidor_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLoginTab();
        objLogin = new ArbetsformedlingenLoginExplorer11(driver);
        objLogin.waitForLoginPageToLoad();
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareExplorer11(driver);
        objLoginArbetsgivare.waitForLoginArbetsgivareTabToLoad();
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLoginButton();

        objMinaSidor = new ArbetsformedlingenMinaSidorExplorer11(driver);
        objMinaSidor.waitForMinaSidorPageToLoad();
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
    }
    @Test
    public void test_Arbetsformedlingen_Arbetsgivare_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLoginTab();
        objLogin = new ArbetsformedlingenLoginExplorer11(driver);
        objLogin.waitForLoginPageToLoad();
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareExplorer11(driver);
        objLoginArbetsgivare.waitForLoginArbetsgivareTabToLoad();
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLoginButton();
        objMinaSidor = new ArbetsformedlingenMinaSidorExplorer11(driver);
        objMinaSidor.waitForMinaSidorPageToLoad();
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickFörArbetsgivareLink();

        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareExplorer11(driver);
        objForArbetsgivare.waitForArbetsgivareLinkToLoad();
        String forAbetsgivareHeader = objForArbetsgivare.getForArbetsgivareHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare!"));
    }
    @Test
    public void test_Arbetsformedlingen_Annonsera_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLoginTab();
        objLogin = new ArbetsformedlingenLoginExplorer11(driver);
        objLogin.waitForLoginPageToLoad();
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareExplorer11(driver);
        objLoginArbetsgivare.waitForLoginArbetsgivareTabToLoad();
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLoginButton();
        objMinaSidor = new ArbetsformedlingenMinaSidorExplorer11(driver);
        objMinaSidor.waitForMinaSidorPageToLoad();
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickFörArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareExplorer11(driver);
        objForArbetsgivare.waitForArbetsgivareLinkToLoad();
        String forAbetsgivareHeader = objForArbetsgivare.getForArbetsgivareHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();

        objAnnonsera = new ArbetsformedlingenAnnonseraExplorer11(driver);
        objAnnonsera.waitForAnnonseraPageToLoad();
        String annonseraHeader = objAnnonsera.getAnnonseraHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
    }
    @Test
    public void test_Arbetsformedlingen_Hantera_Annonser_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLoginTab();
        objLogin = new ArbetsformedlingenLoginExplorer11(driver);
        objLogin.waitForLoginPageToLoad();
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareExplorer11(driver);
        objLoginArbetsgivare.waitForLoginArbetsgivareTabToLoad();
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLoginButton();
        objMinaSidor = new ArbetsformedlingenMinaSidorExplorer11(driver);
        objMinaSidor.waitForMinaSidorPageToLoad();
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickFörArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareExplorer11(driver);
        objForArbetsgivare.waitForArbetsgivareLinkToLoad();
        String forAbetsgivareHeader = objForArbetsgivare.getForArbetsgivareHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraExplorer11(driver);
        objAnnonsera.waitForAnnonseraPageToLoad();
        String annonseraHeader = objAnnonsera.getAnnonseraHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickAnnonseraIPlatsbankenButton();

        objHanteraAnnonser = new ArbetsformedlingenHanteraAnnonserExplorer11(driver);
        objHanteraAnnonser.waitForHanteraAnnonserPageToLoad();
        String hanteraAnnonserHeader = objHanteraAnnonser.getHanteraAnnonserHeader();
        Assert.assertTrue(hanteraAnnonserHeader.toLowerCase().contains("hantera dina och organisationens annonser"));
    }
    @Test
    public void test_Arbetsformedlingen_Skapa_Annons_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLoginTab();
        objLogin = new ArbetsformedlingenLoginExplorer11(driver);
        objLogin.waitForLoginPageToLoad();
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareExplorer11(driver);
        objLoginArbetsgivare.waitForLoginArbetsgivareTabToLoad();
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLoginButton();
        objMinaSidor = new ArbetsformedlingenMinaSidorExplorer11(driver);
        objMinaSidor.waitForMinaSidorPageToLoad();
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickFörArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareExplorer11(driver);
        objForArbetsgivare.waitForArbetsgivareLinkToLoad();
        String forAbetsgivareHeader = objForArbetsgivare.getForArbetsgivareHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraExplorer11(driver);
        objAnnonsera.waitForAnnonseraPageToLoad();
        String annonseraHeader = objAnnonsera.getAnnonseraHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickAnnonseraIPlatsbankenButton();
        objHanteraAnnonser = new ArbetsformedlingenHanteraAnnonserExplorer11(driver);
        objHanteraAnnonser.waitForHanteraAnnonserPageToLoad();
        String hanteraAnnonserHeader = objHanteraAnnonser.getHanteraAnnonserHeader();
        Assert.assertTrue(hanteraAnnonserHeader.toLowerCase().contains("hantera dina och organisationens annonser"));
        objHanteraAnnonser.clickSkapaAnnonsButton();

        objSkapaAnnons = new ArbetsformedlingenSkapaAnnonsExplorer11(driver);
        objSkapaAnnons.waitForSkapaAnnonsPageToLoad();
        String skapaAnnonsHeader = objSkapaAnnons.getSkapaAnnonsHeader();
        Assert.assertTrue(skapaAnnonsHeader.toLowerCase().contains("skapa annons"));
    }
    //------------------------------------------------------------------------------------------------------------------
    //NOT LOGGED IN TESTS
    @Test
    public void test_Arbetsformedlingen_Annonsera() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareExplorer11(driver);
        objForArbetsgivare.waitForArbetsgivareLinkToLoad();
        String forArbetsgivareHeader = objForArbetsgivare.getForArbetsgivareHeader();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();

        objAnnonsera = new ArbetsformedlingenAnnonseraExplorer11(driver);
        objAnnonsera.waitForAnnonseraPageToLoad();
        String annonseraHeader = objAnnonsera.getAnnonseraHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
    }
    @Test
    public void test_Arbetsformedlingen_AnnonseringsvillkorFörPlatsbanken() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareExplorer11(driver);
        objForArbetsgivare.waitForArbetsgivareLinkToLoad();
        String forArbetsgivareHeader = objForArbetsgivare.getForArbetsgivareHeader();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraExplorer11(driver);
        objAnnonsera.waitForAnnonseraPageToLoad();
        String annonseraHeader = objAnnonsera.getAnnonseraHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickLäsVåraVillkorFörAttAnnonseraLink();

        objAnnonseringsvillkor = new ArbetsformedlingenAnnonseringsvillkorExplorer11(driver);
        objAnnonseringsvillkor.waitForAnnonseringsVillkorPageToLoad();
        String annonseringsVillkorHeader = objAnnonseringsvillkor.getAnnonseringsvillkorHeader();
        Assert.assertTrue(annonseringsVillkorHeader.toLowerCase().contains("annonseringsvillkor för platsbanken"));
    }
    @Test
    public void test_Arbetsformedlingen_Direktöverförda_Annonser() {
        objHomePage = new ArbetsformedlingenHomePageExplorer11(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareExplorer11(driver);
        objForArbetsgivare.waitForArbetsgivareLinkToLoad();
        String forArbetsgivareHeader = objForArbetsgivare.getForArbetsgivareHeader();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraExplorer11(driver);
        objAnnonsera.waitForAnnonseraPageToLoad();
        String annonseraHeader = objAnnonsera.getAnnonseraHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.waitForAnnonseraPageToLoadClickable();
        objAnnonsera.clickDirektöverfördaAnnonserButton();

        objDirektöverfördaAnnonser = new ArbetsformedlingenDirektoverfordaAnnonserExplorer11(driver);
        objDirektöverfördaAnnonser.waitForDirektöverfördaAnnonserPageToLoad();
        String direktöverfördaAnnonserHeader = objDirektöverfördaAnnonser.getDirektöverfördaAnnonserHeader();
        Assert.assertTrue(direktöverfördaAnnonserHeader.toLowerCase().contains("överför annonser från ditt rekryteringsverktyg till platsbanken"));
    }
}
