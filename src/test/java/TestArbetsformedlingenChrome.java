import arbetsformedlingenPagesChrome.ArbetsformedlingenHanteraAnnonserChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenHomePageChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenLoginChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenLoginArbetsgivareChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenMinaSidorChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenForArbetsgivareChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenAnnonseraChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenSkapaAnnonsChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenAnnonseringsvillkorChrome;
import arbetsformedlingenPagesChrome.ArbetsformedlingenDirektoverfordaAnnonserChrome;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

//import java.util.concurrent.TimeUnit;

public class TestArbetsformedlingenChrome {
    WebDriver driver;
    ArbetsformedlingenHomePageChrome objHomePage;
    ArbetsformedlingenLoginChrome objLogin;
    ArbetsformedlingenLoginArbetsgivareChrome objLoginArbetsgivare;
    ArbetsformedlingenMinaSidorChrome objMinaSidor;
    ArbetsformedlingenHanteraAnnonserChrome objHanteraAnnonser;
    ArbetsformedlingenSkapaAnnonsChrome objSkapaAnnons;
    ArbetsformedlingenForArbetsgivareChrome objForArbetsgivare;
    ArbetsformedlingenAnnonseraChrome objAnnonsera;
    ArbetsformedlingenAnnonseringsvillkorChrome objAnnonseringsvillkor;
    ArbetsformedlingenDirektoverfordaAnnonserChrome objDirektöverfördaAnnonser;

    @BeforeEach
    public void setup() {
        WebDriverManager.chromedriver().setup();
        //driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("https://arbetsformedlingen.se/");
    }
    @AfterEach
    public void closeDriver() { driver.close(); }
     @Test
     public void test_Arbetsformedlingen_Home_Page_Navigates_Properly() {
         objHomePage = new ArbetsformedlingenHomePageChrome(driver);
         String homePageHeader = objHomePage.getHomePageHeader();
         Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
     }
     @Test
     public void test_Arbetsformedlingen_Login_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();

        objLogin = new ArbetsformedlingenLoginChrome(driver);
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
     }
     @Test
     public void test_Arbetsformedlingen_Login_Arbetsgivare_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginChrome(driver);
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();

        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareChrome(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
     }
    @Test
     public void test_Arbetsformedlingen_Mina_Sidor_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginChrome(driver);
        objLogin = new ArbetsformedlingenLoginChrome(driver);
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareChrome(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();

        objMinaSidor = new ArbetsformedlingenMinaSidorChrome(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
     }
    @Test
    public void test_Arbetsformedlingen_Arbetsgivare_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginChrome(driver);
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareChrome(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorChrome(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();

        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareChrome(driver);
        String forAbetsgivareHeader = objForArbetsgivare.getAnnonseraHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
    }
    @Test
    public void test_Arbetsformedlingen_Annonsera_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginChrome(driver);
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareChrome(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorChrome(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareChrome(driver);
        String forAbetsgivareHeader = objForArbetsgivare.getAnnonseraHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();

        objAnnonsera = new ArbetsformedlingenAnnonseraChrome(driver);
        String annonseraHeader = objAnnonsera.getAnnonseranHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
    }
    @Test
    public void test_Arbetsformedlingen_Hantera_Annonser_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginChrome(driver);
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareChrome(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorChrome(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareChrome(driver);
        String forAbetsgivareHeader = objForArbetsgivare.getAnnonseraHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraChrome(driver);
        String annonseraHeader = objAnnonsera.getAnnonseranHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickAnnonseraIPlatsbankenButton();

        objHanteraAnnonser = new ArbetsformedlingenHanteraAnnonserChrome(driver);
        String hanteraAnnonserHeader = objHanteraAnnonser.getHanteraAnnonserHeader();
        Assert.assertTrue(hanteraAnnonserHeader.toLowerCase().contains("hantera dina och organisationens annonser"));
    }
    @Test
    public void test_Arbetsformedlingen_Skapa_Annons_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginChrome(driver);
        String loginHeader = objLogin.getLoginHeader();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareChrome(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeader();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorChrome(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeader();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareChrome(driver);
        String forAbetsgivareHeader = objForArbetsgivare.getAnnonseraHeader();
        Assert.assertTrue(forAbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraChrome(driver);
        String annonseraHeader = objAnnonsera.getAnnonseranHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickAnnonseraIPlatsbankenButton();
        objHanteraAnnonser = new ArbetsformedlingenHanteraAnnonserChrome(driver);
        String hanteraAnnonserHeader = objHanteraAnnonser.getHanteraAnnonserHeader();
        Assert.assertTrue(hanteraAnnonserHeader.toLowerCase().contains("hantera dina och organisationens annonser"));
        objHanteraAnnonser.clickSkapaAnnonsButton();

        objSkapaAnnons = new ArbetsformedlingenSkapaAnnonsChrome(driver);
        objSkapaAnnons.waitForSkapaAnnonsPageToLoad();
        String skapaAnnonsHeader = objSkapaAnnons.getSkapaAnnonsHeader();
        Assert.assertTrue(skapaAnnonsHeader.toLowerCase().contains("skapa annons"));
     }
    //------------------------------------------------------------------------------------------------------------------
     //NOT LOGGED IN TESTS
     @Test
     public void test_Arbetsformedlingen_Annonsera() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareChrome(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeader();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();

        objAnnonsera = new ArbetsformedlingenAnnonseraChrome(driver);
        String annonseraHeader = objAnnonsera.getAnnonseranHeader();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
     }
     @Test
     public void test_Arbetsformedlingen_AnnonseringsvillkorFörPlatsbanken() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareChrome(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeader();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraChrome(driver);
        objAnnonsera.clickLäsVåraVillkorFörAttAnnonseraLink();

        objAnnonseringsvillkor = new ArbetsformedlingenAnnonseringsvillkorChrome(driver);
        String annonseringsvillkorHeaderText = objAnnonseringsvillkor.getAnnonseringsvillkorHeaderText();
        Assert.assertTrue(annonseringsvillkorHeaderText.toLowerCase().contains("annonseringsvillkor för platsbanken"));
     }
     @Test
     public void test_Arbetsformedlingen_Direktöverförda_Annonser() {
        objHomePage = new ArbetsformedlingenHomePageChrome(driver);
        String homePageHeader = objHomePage.getHomePageHeader();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareChrome(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeader();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraChrome(driver);
        objAnnonsera.waitForAnnonseraPageToLoad();
        objAnnonsera.clickJagFörstårButton();
        objAnnonsera.clickDirektöverfördaAnnonserButton();

        objDirektöverfördaAnnonser = new ArbetsformedlingenDirektoverfordaAnnonserChrome(driver);
        String direktöverfördaAnnonserHeaderText = objDirektöverfördaAnnonser.getDirektöverfördaAnnonserHeaderText();
        Assert.assertTrue(direktöverfördaAnnonserHeaderText.toLowerCase().contains("direktöverförda annonser"));
     }

}
