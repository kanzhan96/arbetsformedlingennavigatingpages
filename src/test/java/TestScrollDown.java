import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestScrollDown {
    WebDriver driver;

    @Test
    public void testScrollDownPage() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\zhake\\IdeaProjects\\Arbetsformedlingen\\src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/test/guru99home/");

        WebElement findText = driver.findElement(By.xpath("//p[contains(text(), '& advance learners')]"));
        js.executeScript("arguments[0].scrollIntoView();", findText);
        driver.close();
    }
}
