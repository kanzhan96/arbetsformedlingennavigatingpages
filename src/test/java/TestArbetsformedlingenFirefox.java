import arbetsformedlingenPagesFirefox.ArbetsformedlingenHomePageFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenLoginFirefox;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.WebDriver;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenHanteraAnnonserFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenLoginArbetsgivareFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenMinaSidorFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenForArbetsgivareFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenAnnonseraFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenSkapaAnnonsFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenAnnonseringsvillkorFirefox;
import arbetsformedlingenPagesFirefox.ArbetsformedlingenDirektoverfordaAnnonserFirefox;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestArbetsformedlingenFirefox {
    WebDriver driver;
    ArbetsformedlingenHomePageFirefox objHomePage;
    ArbetsformedlingenLoginFirefox objLogin;
    ArbetsformedlingenLoginArbetsgivareFirefox objLoginArbetsgivare;
    ArbetsformedlingenMinaSidorFirefox objMinaSidor;
    ArbetsformedlingenHanteraAnnonserFirefox objHanteraAnnonser;
    ArbetsformedlingenSkapaAnnonsFirefox objSkapaAnnons;
    ArbetsformedlingenForArbetsgivareFirefox objForArbetsgivare;
    ArbetsformedlingenAnnonseraFirefox objAnnonsera;
    ArbetsformedlingenAnnonseringsvillkorFirefox objAnnonseringsvillkor;
    ArbetsformedlingenDirektoverfordaAnnonserFirefox objDirektöverfördaAnnonser;

    @BeforeEach
    public void setup() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://arbetsformedlingen.se/");
    }
    @AfterEach
    public void closeDriver() {driver.close();}
    @Test
    public void test_Arbetsformedlingen_Home_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
   }
    @Test
    public void test_Arbetsformedlingen_Login_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();

        objLogin = new ArbetsformedlingenLoginFirefox(driver);
        String loginHeader = objLogin.getLoginHeaderText();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
    }
    @Test
    public void test_Arbetsformedlingen_Login_Arbetsgivare_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginFirefox(driver);
        String loginHeader = objLogin.getLoginHeaderText();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();

        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareFirefox(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeaderText();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
    }
    @Test
    public void test_Arbetsformedlingen_Mina_Sidor_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginFirefox(driver);
        String loginHeader = objLogin.getLoginHeaderText();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareFirefox(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeaderText();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();

        objMinaSidor = new ArbetsformedlingenMinaSidorFirefox(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeaderText();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
    }
    @Test
    public void test_Arbetsformedlingen_For_Arbetsgivare_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginFirefox(driver);
        String loginHeader = objLogin.getLoginHeaderText();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareFirefox(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeaderText();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorFirefox(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeaderText();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();

        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareFirefox(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeaderText();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
    }
    @Test
    public void test_Arbetsformedlingen_Annonsera_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginFirefox(driver);
        String loginHeader = objLogin.getLoginHeaderText();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareFirefox(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeaderText();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorFirefox(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeaderText();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareFirefox(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeaderText();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();

        objAnnonsera = new ArbetsformedlingenAnnonseraFirefox(driver);
        String annonseraHeader = objAnnonsera.getAnnonseraHeaderText();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
    }
    @Test
    public void test_Arbetsformedlingen_Hantera_Annons_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginFirefox(driver);
        String loginHeader = objLogin.getLoginHeaderText();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareFirefox(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeaderText();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorFirefox(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeaderText();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareFirefox(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeaderText();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraFirefox(driver);
        String annonseraHeader = objAnnonsera.getAnnonseraHeaderText();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickAnnonseraIPlatsbankenButton();

        objHanteraAnnonser = new ArbetsformedlingenHanteraAnnonserFirefox(driver);
        String hanteraAnnonserHeader = objHanteraAnnonser.getHanteraAnnonserHeaderText();
        Assert.assertTrue(hanteraAnnonserHeader.toLowerCase().contains("hantera dina och organisationens annonser"));
    }

    @Test
    public void test_Arbetsformedlingen_Skapa_Annons_Page_Navigates_Properly() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickLogin();
        objLogin = new ArbetsformedlingenLoginFirefox(driver);
        String loginHeader = objLogin.getLoginHeaderText();
        Assert.assertTrue(loginHeader.toLowerCase().contains("hur vill du logga in?"));
        objLogin.clickArbetsgivareTab();
        objLoginArbetsgivare = new ArbetsformedlingenLoginArbetsgivareFirefox(driver);
        String loginArbetsgivareHeader = objLoginArbetsgivare.getLoginArbetsgivareHeaderText();
        Assert.assertTrue(loginArbetsgivareHeader.toLowerCase().contains("logga in som arbetsgivare"));
        objLoginArbetsgivare.loginArbetsgivare("Utv_TestBengt", "Abcd1234");
        objLoginArbetsgivare.clickLogin();
        objMinaSidor = new ArbetsformedlingenMinaSidorFirefox(driver);
        String minaSidorHeader = objMinaSidor.getMinaSidorHeaderText();
        Assert.assertTrue(minaSidorHeader.toLowerCase().contains("mina sidor"));
        objMinaSidor.clickForArbetsgivareLink();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareFirefox(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeaderText();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraFirefox(driver);
        String annonseraHeader = objAnnonsera.getAnnonseraHeaderText();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickAnnonseraIPlatsbankenButton();
        objHanteraAnnonser = new ArbetsformedlingenHanteraAnnonserFirefox(driver);
        String hanteraAnnonserHeader = objHanteraAnnonser.getHanteraAnnonserHeaderText();
        Assert.assertTrue(hanteraAnnonserHeader.toLowerCase().contains("hantera dina och organisationens annonser"));
        objHanteraAnnonser.clickSkapaAnnonsButton();

        objSkapaAnnons = new ArbetsformedlingenSkapaAnnonsFirefox(driver);
        objSkapaAnnons.waitForSkapaAnnonsPageToLoad();
        String skapaAnnonsHeader = objSkapaAnnons.getSkapaAnnonsHeaderText();
        Assert.assertTrue(skapaAnnonsHeader.toLowerCase().contains("skapa annons"));
    }
    //------------------------------------------------------------------------------------------------------------------
    //NOT LOGGED IN TESTS
    @Test
    public void test_Arbetsformedlingen_Annonsera() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareFirefox(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeaderText();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();

        objAnnonsera = new ArbetsformedlingenAnnonseraFirefox(driver);
        String annonseraHeader = objAnnonsera.getAnnonseraHeaderText();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
    }
    @Test
    public void test_Arbetsformedlingen_AnnonseringsvillkorFörPlatsbanken() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareFirefox(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeaderText();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraFirefox(driver);
        String annonseraHeader = objAnnonsera.getAnnonseraHeaderText();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.clickLäsVåraVillkorFörAttAnnonseraLink();

        objAnnonseringsvillkor = new ArbetsformedlingenAnnonseringsvillkorFirefox(driver);
        String annonseringsVillkorHeader = objAnnonseringsvillkor.getAnnonseringsvillkorHeaderText();
        Assert.assertTrue(annonseringsVillkorHeader.toLowerCase().contains("annonseringsvillkor för platsbanken"));
    }
    @Test
    public void test_Arbetsformedlingen_Direktöverförda_Annonser() {
        objHomePage = new ArbetsformedlingenHomePageFirefox(driver);
        String homePageHeader = objHomePage.getHomePageHeaderText();
        Assert.assertTrue(homePageHeader.toLowerCase().contains("välkommen till hela sveriges arbetsförmedlingen.se"));
        objHomePage.clickFörArbetsgivareButton();
        objForArbetsgivare = new ArbetsformedlingenForArbetsgivareFirefox(driver);
        String forArbetsgivareHeader = objForArbetsgivare.getAnnonseraHeaderText();
        Assert.assertTrue(forArbetsgivareHeader.toLowerCase().contains("välkommen arbetsgivare"));
        objForArbetsgivare.clickAnnonseraButton();
        objAnnonsera = new ArbetsformedlingenAnnonseraFirefox(driver);
        String annonseraHeader = objAnnonsera.getAnnonseraHeaderText();
        Assert.assertTrue(annonseraHeader.toLowerCase().contains("nå ut till fler med sveriges största jobbsöksajt"));
        objAnnonsera.waitForAnnonseraPageToLoad();
        objAnnonsera.clickJagFörstårButton();
        objAnnonsera.clickDirektöverfördaAnnonserButton();

        objDirektöverfördaAnnonser = new ArbetsformedlingenDirektoverfordaAnnonserFirefox(driver);
        String direktöverfördaAnnonserHeader = objDirektöverfördaAnnonser.getDirektöverfördaAnnonserHeaderText();
        Assert.assertTrue(direktöverfördaAnnonserHeader.toLowerCase().contains("direktöverförda annonser"));
    }

}
