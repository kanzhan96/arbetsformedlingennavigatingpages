package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenHomePageExplorer11 {
    WebDriver driver;
    By login = By.xpath("//a[@href='https://www.arbetsformedlingen.se/loggain']");
    By homePageHeader =  By.id("h-ValkommentillhelaSverigesarbetsformedlingense");
    By förAbetsgivareButton = By.xpath("//a[@class='btn-app-link btn-app-link--ghost']");

    public ArbetsformedlingenHomePageExplorer11(WebDriver driver) { this.driver = driver; }

    public String getHomePageHeader() { return driver.findElement(homePageHeader).getText();}

    public void clickLoginTab() { driver.findElement(login).click();}

    public void clickFörArbetsgivareButton() { driver.findElement(förAbetsgivareButton).click(); }
}
