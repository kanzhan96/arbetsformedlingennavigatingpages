package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenAnnonseringsvillkorExplorer11 {
    WebDriver driver;
    By annonseringsvillkorHeader = By.xpath("//h1[@class='heading']");

    public ArbetsformedlingenAnnonseringsvillkorExplorer11(WebDriver driver) { this.driver = driver; }

    public String getAnnonseringsvillkorHeader() { return driver.findElement(annonseringsvillkorHeader).getText();}

    public void waitForAnnonseringsVillkorPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(annonseringsvillkorHeader, "Annonseringsvillkor för Platsbanken"));
    }
}
