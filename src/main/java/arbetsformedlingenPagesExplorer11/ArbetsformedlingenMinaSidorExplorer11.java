package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenMinaSidorExplorer11 {
    WebDriver driver;
    By minaSidorHeader = By.className("heading");
    By förArbetsgivareLink = By.xpath("//a[@class='tracemeny']");

    public ArbetsformedlingenMinaSidorExplorer11(WebDriver driver) { this.driver = driver; }

    public String getMinaSidorHeader() { return driver.findElement(minaSidorHeader).getText();}

    public void clickFörArbetsgivareLink() { driver.findElement(förArbetsgivareLink).click();}

    public void waitForMinaSidorPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(minaSidorHeader, "Mina sidor"));
    }
}
