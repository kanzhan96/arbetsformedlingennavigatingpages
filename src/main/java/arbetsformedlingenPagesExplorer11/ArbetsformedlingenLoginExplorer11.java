package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenLoginExplorer11 {
    WebDriver driver;
    By arbetsgivareTab = By.xpath("//*[@href='/loggain/arbetsgivare']");
    By loginHeaderText = By.xpath("//h1[contains(text(), 'Hur vill du logga in?')]");
    By arbetssökandeTab = By.xpath("//a[@href='/loggain']");

    public ArbetsformedlingenLoginExplorer11(WebDriver driver) { this.driver = driver; }

    public String getLoginHeader() { return driver.findElement(loginHeaderText).getText(); }

    public void clickArbetsgivareTab() { driver.findElement(arbetsgivareTab).click();}

    public void waitForLoginPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(arbetssökandeTab, "Arbetssökande"));
    }
}
