package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenAnnonseraExplorer11 {
    WebDriver driver;
    By annonseraHeader = By.xpath("//h1[contains(text(), 'Nå ut till fler med Sveriges största jobbsöksajt')]");
    By annonseraIPlatsbankenButton = By.xpath("//button[@title='Annonsera i platsbanken.']");
    By läsVåraVillkorFörAttAnnonseraLink = By.xpath("//a[@href='/For-arbetsgivare/Annonsera/Annonseringsvillkor']");
    By jagFörstårButton = By.xpath("//a[@aria-label='Jag förstår. Meddelandet om cookies kommer inte visas igen.']");
    By direktöverfördaAnnonserButton = By.xpath("//button[@aria-label='Gå vidare till direktöverförda annonser för att se aktuella och testannonser.']");

    public ArbetsformedlingenAnnonseraExplorer11(WebDriver driver) { this.driver = driver; }

    public String getAnnonseraHeader() { return driver.findElement(annonseraHeader).getText();}

    public void clickAnnonseraIPlatsbankenButton() { driver.findElement(annonseraIPlatsbankenButton).click();}

    public void clickLäsVåraVillkorFörAttAnnonseraLink() { driver.findElement(läsVåraVillkorFörAttAnnonseraLink).click();}

    public void waitForAnnonseraPageToLoadClickable() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(direktöverfördaAnnonserButton));
    }

    public void waitForAnnonseraPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(annonseraHeader, "Nå ut till fler med Sveriges största jobbsöksajt"));
    }

    public void clickDirektöverfördaAnnonserButton() { driver.findElement(direktöverfördaAnnonserButton).click();}
}
