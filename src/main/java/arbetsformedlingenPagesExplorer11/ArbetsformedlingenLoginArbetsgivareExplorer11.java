package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenLoginArbetsgivareExplorer11 {
    WebDriver driver;
    By loginArbetsgivareHeader = By.xpath("//h1[contains(text(), 'Logga in som arbetsgivare')]");
    By username = By.id("username");
    By password = By.id("txtLosen");
    By loginButton = By.xpath("//button[@class = 'btn btn-primary']");

    public ArbetsformedlingenLoginArbetsgivareExplorer11(WebDriver driver) { this.driver = driver; }

    public String getLoginArbetsgivareHeader() { return driver.findElement(loginArbetsgivareHeader).getText();}

    public void setUserName(String strUserName) { driver.findElement(username).sendKeys(strUserName); }

    public void setPassword(String strPassword) { driver.findElement(password).sendKeys(strPassword); }

    public void clickLoginButton() { driver.findElement(loginButton).click();}

    public void loginArbetsgivare(String strUserName, String strPassword) {
        this.setUserName(strUserName);
        this.setPassword(strPassword);
    }

    public void waitForLoginArbetsgivareTabToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(loginArbetsgivareHeader, "Logga in som arbetsgivare"));
    }
}
