package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenDirektoverfordaAnnonserExplorer11 {
    WebDriver driver;
    By direktöverfördaAnnonserHeader = By.xpath("//h1[@class='heading']");

    public ArbetsformedlingenDirektoverfordaAnnonserExplorer11(WebDriver driver) { this.driver = driver; }

    public String getDirektöverfördaAnnonserHeader() { return driver.findElement(direktöverfördaAnnonserHeader).getText(); }

    public void waitForDirektöverfördaAnnonserPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(direktöverfördaAnnonserHeader, "Överför annonser från ditt rekryteringsverktyg till Platsbanken"));
    }
}
