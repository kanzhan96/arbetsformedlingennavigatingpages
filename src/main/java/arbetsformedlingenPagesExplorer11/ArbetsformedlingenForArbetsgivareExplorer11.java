package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenForArbetsgivareExplorer11 {
    WebDriver driver;
    By forArbetsgivareHeader = By.xpath("//h1[@class='heading']");
    By annonseraButton = By.xpath("//a[@href='/for-arbetsgivare/annonsera']");

    public ArbetsformedlingenForArbetsgivareExplorer11(WebDriver driver) { this.driver = driver; }

    public String getForArbetsgivareHeader() { return driver.findElement(forArbetsgivareHeader).getText();}

    public void clickAnnonseraButton() { driver.findElement(annonseraButton).click();}

    public void waitForArbetsgivareLinkToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(forArbetsgivareHeader, "Välkommen arbetsgivare!"));
    }
}
