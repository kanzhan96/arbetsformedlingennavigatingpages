package arbetsformedlingenPagesExplorer11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenHanteraAnnonserExplorer11 {
    WebDriver driver;
    By hanteraAnnonserHeader = By.xpath("//h1[@class='d-none d-sm-block']");
    By skapaAnnonsButton = By.xpath("//button[@class='btn btn-primary']");

    public ArbetsformedlingenHanteraAnnonserExplorer11(WebDriver driver) { this.driver = driver; }

    public String getHanteraAnnonserHeader() { return driver.findElement(hanteraAnnonserHeader).getText();}

    public void clickSkapaAnnonsButton() { driver.findElement(skapaAnnonsButton).click();}

    public void waitForHanteraAnnonserPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(hanteraAnnonserHeader, "Hantera dina och organisationens annonser"));
    }
}
