package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenHanteraAnnonserFirefox {
    WebDriver driver;
    By hanteraAnnonserHeaderText = By.xpath("//h1[contains(text(), 'Hantera dina och organisationens annonser')]");
    By skapaAnnonsButton = By.xpath("//button[@aria-label='Skapa annons']");

    public ArbetsformedlingenHanteraAnnonserFirefox(WebDriver driver) { this.driver = driver; }

    public String getHanteraAnnonserHeaderText() { return driver.findElement(hanteraAnnonserHeaderText).getText();}

    public void clickSkapaAnnonsButton() { driver.findElement(skapaAnnonsButton).click();}
}
