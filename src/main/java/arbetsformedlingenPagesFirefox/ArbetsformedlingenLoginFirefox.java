package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenLoginFirefox {
    WebDriver driver;
    By loginHeaderText = By.xpath("//h1[@id='h-Hurvillduloggain']");
    By arbetsgivareTab = By.xpath("//a[@href='/loggain/arbetsgivare']");

    public ArbetsformedlingenLoginFirefox(WebDriver driver) { this.driver = driver;}

    public String getLoginHeaderText() { return driver.findElement(loginHeaderText).getText();}

    public void clickArbetsgivareTab() { driver.findElement(arbetsgivareTab).click();}
}
