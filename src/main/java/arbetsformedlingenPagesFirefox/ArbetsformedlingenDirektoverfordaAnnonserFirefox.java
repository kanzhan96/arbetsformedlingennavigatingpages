package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenDirektoverfordaAnnonserFirefox {
    WebDriver driver;
    By direktöverfördaAnnonserHeaderText = By.xpath("//h1[@class='heading']");

    public ArbetsformedlingenDirektoverfordaAnnonserFirefox(WebDriver driver) { this.driver = driver; }

    public String getDirektöverfördaAnnonserHeaderText() { return driver.findElement(direktöverfördaAnnonserHeaderText).getText(); }
}
