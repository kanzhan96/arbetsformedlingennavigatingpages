package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenForArbetsgivareFirefox {
    WebDriver driver;
    By annonseraHeaderText = By.id("h-Valkommenarbetsgivare");
    By annonseraButton = By.xpath("//a[@href='/for-arbetsgivare/annonsera']");

    public ArbetsformedlingenForArbetsgivareFirefox(WebDriver driver) { this.driver = driver;}

    public String getAnnonseraHeaderText() { return driver.findElement(annonseraHeaderText).getText();}

    public void clickAnnonseraButton() { driver.findElement(annonseraButton).click();}
}
