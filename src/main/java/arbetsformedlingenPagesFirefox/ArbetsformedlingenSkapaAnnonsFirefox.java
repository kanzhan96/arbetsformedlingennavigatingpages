package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenSkapaAnnonsFirefox {
    WebDriver driver;
    By skapaAnnonsHeaderText = By.xpath("//h1[contains(text(), 'Skapa Annons')]");

    public ArbetsformedlingenSkapaAnnonsFirefox(WebDriver driver) { this.driver = driver; }

    public String getSkapaAnnonsHeaderText() { return driver.findElement(skapaAnnonsHeaderText).getText();}

    public void waitForSkapaAnnonsPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(skapaAnnonsHeaderText, "Skapa Annons"));
    }
}
