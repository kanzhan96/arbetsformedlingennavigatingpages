package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenLoginArbetsgivareFirefox {
    WebDriver driver;
    By loginArbetsgivareHeaderText = By.xpath("//h1[@id='h-Loggainsomarbetsgivare']");
    By username = By.id("username");
    By password = By.id("txtLosen");
    By loginButton = By.xpath("//button[@class = 'btn btn-primary']");

    public ArbetsformedlingenLoginArbetsgivareFirefox(WebDriver driver) { this.driver = driver;}

    public String getLoginArbetsgivareHeaderText() { return driver.findElement(loginArbetsgivareHeaderText).getText();}

    public void setUsername(String strUsername) { driver.findElement(username).sendKeys(strUsername);}

    public void setPassword(String strPassword) { driver.findElement(password).sendKeys(strPassword);}

    public void clickLogin() { driver.findElement(loginButton).click();}

    public void loginArbetsgivare(String strUserName, String strPassword) {
        this.setUsername(strUserName);
        this.setPassword(strPassword);
    }
}
