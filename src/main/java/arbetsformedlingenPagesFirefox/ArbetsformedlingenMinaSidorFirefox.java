package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenMinaSidorFirefox {
    WebDriver driver;
    By minaSidorHeaderText = By.xpath("//h1[@class='heading']");
    By förArbetsgivareLink = By.xpath("//a[@href='https://arbetsformedlingen.se/for-arbetsgivare']");

    public ArbetsformedlingenMinaSidorFirefox(WebDriver driver) { this.driver = driver;}

    public String getMinaSidorHeaderText() { return driver.findElement(minaSidorHeaderText).getText();}

    public void clickForArbetsgivareLink() { driver.findElement(förArbetsgivareLink).click();}
}
