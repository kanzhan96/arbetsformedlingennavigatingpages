package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenHomePageFirefox {
    WebDriver driver;
    By login = By.xpath("//a[@class='functions-nav__link js-start-login']");
    By homePageHeaderText = By.xpath("//*[contains(text(), 'Välkommen till hela Sveriges arbetsförmedlingen.se')]");
    By förArbetsgivareButton = By.xpath("//a[@class='btn-app-link btn-app-link--ghost']");

    public ArbetsformedlingenHomePageFirefox(WebDriver driver) { this.driver = driver;}

    public String getHomePageHeaderText() { return driver.findElement(homePageHeaderText).getText();}

    public void clickLogin() { driver.findElement(login).click();}

    public void clickFörArbetsgivareButton() { driver.findElement(förArbetsgivareButton).click();}
}
