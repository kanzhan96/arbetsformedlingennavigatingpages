package arbetsformedlingenPagesFirefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenAnnonseringsvillkorFirefox {
    WebDriver driver;
    By annonseringsvillkorHeaderText = By.xpath("//h1[@class='heading']");

    public ArbetsformedlingenAnnonseringsvillkorFirefox(WebDriver driver) { this.driver = driver; }

    public String getAnnonseringsvillkorHeaderText() { return driver.findElement(annonseringsvillkorHeaderText).getText();}
}
