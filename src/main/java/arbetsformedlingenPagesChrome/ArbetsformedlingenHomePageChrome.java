package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenHomePageChrome {
    WebDriver driver;
    By login = By.xpath("//a[contains(@href, 'https://www.arbetsformedlingen.se/loggain')]");
    By homePageHeaderText = By.xpath("//*[contains(text(), 'Välkommen till hela Sveriges arbetsförmedlingen.se')]");
    By förAbetsgivareButton = By.xpath("//a[@class='btn-app-link btn-app-link--ghost']");

    public ArbetsformedlingenHomePageChrome(WebDriver driver) { this.driver = driver;}

    public String getHomePageHeader() { return driver.findElement(homePageHeaderText).getText();}

    public void clickLogin() { driver.findElement(login).click();}

    public void clickFörArbetsgivareButton() { driver.findElement(förAbetsgivareButton).click();}

}
