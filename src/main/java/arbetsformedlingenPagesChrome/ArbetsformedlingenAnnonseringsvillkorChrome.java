package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenAnnonseringsvillkorChrome {
    WebDriver driver;
    By annonseringsvillkorHeaderText = By.xpath("//h1[@class='heading']");

    public ArbetsformedlingenAnnonseringsvillkorChrome(WebDriver driver) { this.driver = driver;}

    public String getAnnonseringsvillkorHeaderText() { return driver.findElement(annonseringsvillkorHeaderText).getText();}
}
