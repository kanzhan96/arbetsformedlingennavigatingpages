package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenLoginArbetsgivareChrome {
    WebDriver driver;
    By loginHeaderArbetsgivareText = By.xpath("//h1[contains(text(), 'Logga in som arbetsgivare')]");
    By username = By.id("username");
    By password = By.id("txtLosen");
    By loginButton = By.xpath("//button[@class = 'btn btn-primary']");

    public ArbetsformedlingenLoginArbetsgivareChrome(WebDriver driver) { this.driver = driver;}

    public String getLoginArbetsgivareHeader() { return driver.findElement(loginHeaderArbetsgivareText).getText();}

    //Set User Name in textbox
    public void setUserName(String strUserName) { driver.findElement(username).sendKeys(strUserName); }

    //Set password in password textbox
    public void setPassword(String strPassword) { driver.findElement(password).sendKeys(strPassword); }

    public void clickLogin() { driver.findElement(loginButton).click();}

    public void loginArbetsgivare(String strUserName, String strPassword) {
        this.setUserName(strUserName);
        this.setPassword(strPassword);
    }
}
