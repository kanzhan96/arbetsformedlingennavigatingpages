package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenLoginChrome {
    WebDriver driver;
    By arbetsgivareTab = By.xpath("//*[@href='/loggain/arbetsgivare']");
    By loginHeaderText = By.xpath("//h1[contains(text(), 'Hur vill du logga in?')]");

    public ArbetsformedlingenLoginChrome(WebDriver driver) { this.driver = driver;}

    public String getLoginHeader() { return driver.findElement(loginHeaderText).getText(); }

    public void clickArbetsgivareTab() { driver.findElement(arbetsgivareTab).click();}

}
