package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenHanteraAnnonserChrome {
    WebDriver driver;
    By hanteraAnnonserHeaderText = By.xpath("//h1[@class='d-none d-sm-block']");
    By skapaAnnonsButton = By.xpath("//button[@class='btn btn-primary']");

    public ArbetsformedlingenHanteraAnnonserChrome(WebDriver driver) { this.driver = driver;}

    public String getHanteraAnnonserHeader() { return driver.findElement(hanteraAnnonserHeaderText).getText();}

    public void clickSkapaAnnonsButton() { driver.findElement(skapaAnnonsButton).click();}

}
