package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenDirektoverfordaAnnonserChrome {
    WebDriver driver;
    By direktöverfördaAnnonserHeaderText = By.xpath("//h1[@class='heading']");

    public ArbetsformedlingenDirektoverfordaAnnonserChrome(WebDriver driver) { this.driver = driver; }

    public String getDirektöverfördaAnnonserHeaderText() { return driver.findElement(direktöverfördaAnnonserHeaderText).getText(); }
}
