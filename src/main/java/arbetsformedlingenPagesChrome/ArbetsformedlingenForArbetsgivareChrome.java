package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenForArbetsgivareChrome {
    WebDriver driver;
    By annonseraHeader = By.xpath("//h1[@class='heading']");
    By annonseraButton = By.xpath("//a[@href='/for-arbetsgivare/annonsera']");

    public ArbetsformedlingenForArbetsgivareChrome(WebDriver driver) { this.driver = driver;}

    public String getAnnonseraHeader() { return driver.findElement(annonseraHeader).getText();}

    public void clickAnnonseraButton() { driver.findElement(annonseraButton).click();}
}
