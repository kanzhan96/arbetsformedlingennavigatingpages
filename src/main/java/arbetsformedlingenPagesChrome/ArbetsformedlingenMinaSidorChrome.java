package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArbetsformedlingenMinaSidorChrome {
    WebDriver driver;
    //By meny = By.xpath("//a[@class='header-link header-link--menu js-open-menu']");
    //By arbetsgivare = By.xpath("//li[@class='main-menu__item']/a[@href='/For-arbetsgivare.html']");
    //By hanteraAnnonserButton = By.xpath("//a[@href='https://arbetsformedlingen.se/for-arbetsgivare/mina-sidor/hantera-annonser']");
    By minaSidorHeaderText = By.className("heading");
    By förArbetsgivareLink = By.xpath("//a[@class='tracemeny']");

    public ArbetsformedlingenMinaSidorChrome(WebDriver driver) { this.driver = driver;}

    public String getMinaSidorHeader() { return driver.findElement(minaSidorHeaderText).getText();}

    //public void clickHanteraAnnonser() { driver.findElement(hanteraAnnonserButton).click();}

    public void clickForArbetsgivareLink() { driver.findElement(förArbetsgivareLink).click();}

}
