package arbetsformedlingenPagesChrome;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArbetsformedlingenSkapaAnnonsChrome {
    WebDriver driver;
    By skapaAnnonsHeader = By.xpath("//h1[contains(text(), 'Skapa Annons')]");


    public ArbetsformedlingenSkapaAnnonsChrome(WebDriver driver) { this.driver = driver;}

    public String getSkapaAnnonsHeader() { return driver.findElement(skapaAnnonsHeader).getText();}

    public void waitForSkapaAnnonsPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.textToBe(skapaAnnonsHeader, "Skapa Annons"));

    }
}
